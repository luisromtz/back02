package productoAPI.servicios;


import productoAPI.modelo.ProductoModelo;
import productoAPI.repository.ProductoRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service public class ProductoServicio {

    @Autowired
    ProductoRepositorio productoRepository;

    public List<ProductoModelo> findAll() {
        return productoRepository.findAll();
    }

    public Optional<ProductoModelo> findById(String  id) {
        return productoRepository.findById(id);
    }

    public ProductoModelo save(ProductoModelo entity) {
        return productoRepository.save(entity);
    }

    public boolean delete(ProductoModelo entity) {
        try {
            productoRepository.delete(entity);
            return true;
        } catch(Exception ex) {
            return false;
        }
    }
}