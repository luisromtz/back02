package productoAPI.controllers;

import productoAPI.modelo.ProductoModelo;
import productoAPI.servicios.ProductoServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List; import java.util.Optional;

@RestController @RequestMapping("/apitechu/v2")
public class ControladorProducto {
    @Autowired
    ProductoServicio productoService;

    @GetMapping("/productos")
    public List<ProductoModelo> getProductos() {
        return productoService.findAll();
    }

    @GetMapping("/productos/{id}" )
    public Optional<ProductoModelo> getProductoId(@PathVariable String id){
        return productoService.findById(id);
    }

    @PostMapping("/productos")
    public ProductoModelo postProductos(@RequestBody ProductoModelo newProducto){
        productoService.save(newProducto);
        return newProducto;
    }

    @PutMapping("/productos")
    public void putProductos(@RequestBody ProductoModelo productoToUpdate){
        productoService.save(productoToUpdate);
    }

    @DeleteMapping("/productos")
    public boolean deleteProductos(@RequestBody ProductoModelo productoToDelete){
        return productoService.delete(productoToDelete);
    }
}