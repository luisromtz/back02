package productoAPI.repository;

import productoAPI.modelo.ProductoModelo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
@Repository public interface ProductoRepositorio extends MongoRepository<ProductoModelo, String> { }